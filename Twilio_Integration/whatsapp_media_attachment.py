from twilio.rest import Client

# Your Account Sid and Auth Token from twilio.com/console
account_sid = 'Your Twilio SID'
auth_token = 'Your Twilio Auth Token'
client = Client(account_sid, auth_token)

message = client.messages.create(
                              from_='whatsapp:+1_your_number',
                              body='Python Media API runs successfully...',
                              media_url=['https://demo.twilio.com/owl.png'],
                              to='whatsapp:+91_your_number'
                          )

print(message.sid)