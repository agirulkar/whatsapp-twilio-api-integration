from twilio.rest import Client

# Your Account Sid and Auth Token from twilio.com/console
account_sid = 'Your Twilio SID'
auth_token = 'Your Twilio Auth Token'
client = Client(account_sid, auth_token)

message = client.messages.create(
                              from_='whatsapp:+1_your_number',
                              body='My Home Location',
                              to='whatsapp:+91_your_number',
                              persistent_action=['geo:20.931573,77.794535|AakashGanga Home'],

                          )

print(message.sid)